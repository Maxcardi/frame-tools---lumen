<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('threads', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('thread_type_id');
            $table->integer('category_id');
            $table->integer('role_id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('description');
            $table->boolean('isOpened')->default(true);
            $table->boolean('isDeleted')->default(false);
            $table->timestamps();

            $table->foreign('thread_type_id')->references('id')->on('thread_types')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('threads');
    }
}
